package ua.shpp.proxy;

import ua.shpp.abstracts.Image;
import ua.shpp.classes.ProxyImage;

public class AppProxy {
    public static void main(String[] args) {
        Image image = new ProxyImage("myFile");
        image.display();
    }
}