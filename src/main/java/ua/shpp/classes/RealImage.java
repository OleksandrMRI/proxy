package ua.shpp.classes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ua.shpp.abstracts.Image;

public class RealImage implements Image {
    Logger log = LoggerFactory.getLogger(RealImage.class);
    String file;

    public RealImage(String file) {
        this.file = file;
        load();
    }

    void load() {
        log.info("Load file: {}", file);
    }

    @Override
    public void display() {
        log.info("Display file: {}", file);
    }
}
